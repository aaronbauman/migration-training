# Setup

This project provides the requirements for the Drupal 7 to Drupal 8 migration training in a minimum number of steps.  This is the recommended way to participate in the training, but alternative instructions to not use a virtual machine are available at the end of this README.


## Virtual machine setup

  * If not already installed, download and install [Vagrant](https://www.vagrantup.com/downloads.html ) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads ).
  * Ensure you have git; open a command line terminal and type `git --version`.  If you do not get a git version number, [install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git ).
  * `git clone git@gitlab.com:agaric/migration-training.git`
  * `cd migration-training`
  * `vagrant up`


### Where Drupal & Drush are in the VM

Get into the virtual machine with `vagrant ssh`

### Drupal 7

  * Web root: `/var/www/drupalvm/drupal7`
  * Drush command: `drush8`
  * Login: `drush8 @d7 uli`

Note: Yes, Drupal 7 uses *Drush version 8*.  The current version of Drush, version 9, does not work with Drupal 7.

### Drupal 8:

  * Web root: `/var/www/drupalvm/drupal8`
  * Drush command: `drush`

### Helpful commands

Import the configuration for changes to the import module:

`drush config-import --partial --source=modules/custom/ud_d8_upgrade/config/install`


## DIY requirements

A Drupal 7 site, with the following modules available (this database will be re-loaded from our sample source):

```
addressfield ctools date entity entityreference email link telephone token url pathauto views
```

A Drupal 8 site, managed by composer, with the following modules:

```
composer require "drupal/devel:^2.1"
composer require "drupal/address:^1.7"
composer require "drupal/migrate_plus:^4.2"
composer require "drupal/migrate_tools:^4.5"
composer require "drupal/paragraphs:^1.10"
composer require "drupal/social_media_links_field:^2.6"
```

### Repositories:

  * https://gitlab.com/agaric/migration-training
  * https://github.com/dinarcon/ud_d8_upgrade


## Shared notes

Put your own tips and errors or other problems you run into here!

https://pad.drutopia.org/p/nj2020-drupal-migrate
