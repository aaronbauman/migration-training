#!/bin/bash
# https://www.cyberciti.biz/faq/how-do-i-empty-mysql-database/
db="$1"
 
# Detect paths
MYSQL=$(which mysql)
AWK=$(which awk)
GREP=$(which grep)
 
if [ $# -ne 1 ]
then
	echo "Usage: $0 {MySQL-Database-Name}"
	echo "Drops all tables from a MySQL database"
	exit 1
fi
 
TABLES=$($MYSQL $db -e 'show tables' | $AWK '{ print $1}' | $GREP -v '^Tables' )
 
for t in $TABLES
do
	echo "Deleting $t table from $db database..."
	$MYSQL $db -e "drop table $t"
done
